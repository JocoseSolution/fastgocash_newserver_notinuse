﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DMT-Manager/MasterPagefor_money-transfer.master" AutoEventWireup="true" CodeFile="BillPayment.aspx.cs" Inherits="DMT_Manager_BillPayment" %>

<%@ Register Src="~/DMT-Manager/User_Control/ud_Mobile_Recharge.ascx" TagPrefix="uc1" TagName="ud_Mobile_Recharge" %>
<%@ Register Src="~/DMT-Manager/User_Control/ud_DTH_Recharge.ascx" TagPrefix="uc1" TagName="ud_DTH_Recharge" %>
<%@ Register Src="~/DMT-Manager/User_Control/ud_Electricity_Recharge.ascx" TagPrefix="uc1" TagName="ud_Electricity_Recharge" %>
<%@ Register Src="~/DMT-Manager/User_Control/ud_landline.ascx" TagPrefix="uc1" TagName="ud_landline" %>
<%@ Register Src="~/DMT-Manager/User_Control/ud_Insurance.ascx" TagPrefix="uc1" TagName="ud_Insurance" %>
<%@ Register Src="~/DMT-Manager/User_Control/ud_GAS.ascx" TagPrefix="uc1" TagName="ud_GAS" %>
<%@ Register Src="~/DMT-Manager/User_Control/ud_internet_isp.ascx" TagPrefix="uc1" TagName="ud_internet_isp" %>
<%@ Register Src="~/DMT-Manager/User_Control/ud_water.ascx" TagPrefix="uc1" TagName="ud_water" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="custom/css/validationcss.css" rel="stylesheet" />
    <link href="custom/css/datetime.css" rel="stylesheet" />
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <style type="text/css">
        #main-wrapper {
            background: #004c60 !important;
        }
    </style>
    <br />
    <br />
    <div class="col-md-12" style="min-height: 300px;">
        <ul class="nav nav-pills" id="pillsmyTab" role="tablist">
            <li class="nav-item"><a class="nav-link actiontype active" data-valtext="mobile" id="firstTab-Pills" data-toggle="tab" href="#mobileTabPills" role="tab" aria-controls="firstTabPills" aria-selected="true" style="color: #fff !important;">Mobile Recharge</a> </li>
            <li class="nav-item"><a class="nav-link actiontype" data-valtext="dth" id="secondTab-Pills" data-toggle="tab" href="#dthTabPills" role="tab" aria-controls="secondTabPills" aria-selected="false" style="color: #fff !important;">DTH</a> </li>
            <li class="nav-item"><a class="nav-link actiontype" data-valtext="electricity" id="thirdTab-Pills" data-toggle="tab" href="#thirdTabPills" role="tab" aria-controls="thirdTabPills" aria-selected="false" style="color: #fff !important;">Electricity</a> </li>
            <li class="nav-item"><a class="nav-link actiontype" data-valtext="landline" id="fourthTab-Pills" data-toggle="tab" href="#fourthTabPills" role="tab" aria-controls="fourthTabPills" aria-selected="false" style="color: #fff !important;">Landline</a> </li>
            <li class="nav-item"><a class="nav-link actiontype" data-valtext="insurance" id="fifthTab-Pills" data-toggle="tab" href="#fifthTabPills" role="tab" aria-controls="fifthTabPills" aria-selected="false" style="color: #fff !important;">Insurance</a> </li>

            <li class="nav-item"><a class="nav-link actiontype" data-valtext="gas" id="gasTab-Pills" data-toggle="tab" href="#gasTabPills" role="tab" aria-controls="gasTabPills" aria-selected="false" style="color: #fff !important;">Gas</a> </li>
            <li class="nav-item"><a class="nav-link actiontype" data-valtext="internet" id="internetTab-Pills" data-toggle="tab" href="#internetTabPills" role="tab" aria-controls="internetTabPills" aria-selected="false" style="color: #fff !important;">Broadband</a> </li>
            <li class="nav-item"><a class="nav-link actiontype" data-valtext="water" id="waterTab-Pills" data-toggle="tab" href="#waterTabPills" role="tab" aria-controls="waterTabPills" aria-selected="false" style="color: #fff !important;">Water</a> </li>
            
        </ul>
        <br />
        <br />
        <div class="tab-content bg-light shadow-sm rounded py-4 mb-4" id="pillsmyTabContent" style="padding: 10px;">

            <div class="tab-pane fade active show" id="mobileTabPills" role="tabpanel" aria-labelledby="firstTab-Pills">
                <uc1:ud_Mobile_Recharge runat="server" ID="ud_Mobile_Recharge" />
            </div>

            <div class="tab-pane fade" id="dthTabPills" role="tabpanel" aria-labelledby="secondTab-Pills">
                <uc1:ud_DTH_Recharge runat="server" ID="ud_DTH_Recharge" />
            </div>

            <div class="tab-pane fade" id="thirdTabPills" role="tabpanel" aria-labelledby="thirdTab-Pills">
                <uc1:ud_Electricity_Recharge runat="server" ID="ud_Electricity_Recharge" />
            </div>

            <div class="tab-pane fade" id="fourthTabPills" role="tabpanel" aria-labelledby="thirdTab-Pills">
                <uc1:ud_landline runat="server" ID="ud_landline" />
            </div>

            <div class="tab-pane fade" id="fifthTabPills" role="tabpanel" aria-labelledby="thirdTab-Pills">
                <uc1:ud_Insurance runat="server" ID="ud_Insurance" />
            </div>

            <div class="tab-pane fade" id="gasTabPills" role="tabpanel" aria-labelledby="gasTab-Pills">
                <uc1:ud_GAS runat="server" ID="ud_GAS" />
            </div>

            <div class="tab-pane fade" id="internetTabPills" role="tabpanel" aria-labelledby="internetTab-Pills">
                <uc1:ud_internet_isp runat="server" ID="ud_internet_isp" />
            </div>

            <div class="tab-pane fade" id="waterTabPills" role="tabpanel" aria-labelledby="waterTab-Pills">
                <uc1:ud_water runat="server" ID="ud_water" />
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
    <br />

    <button type="button" class="btn btn-info btn-lg successmessage hidden" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#SuccessMsg"></button>
    <div class="modal fade" id="SuccessMsg" role="dialog">
        <div class="modal-dialog modal-md" style="margin: 15% auto!important;">
            <div class="modal-content" style="text-align: center; border-radius: 5px !important;">
                <div class="modal-header" style="padding: 12px;">
                    <h5 class="modal-title modelheading"></h5>
                </div>
                <div class="modal-body" style="padding-left: 20px!important;">
                    <h6 class="sucessmsg"></h6>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" style="padding: 0.5rem 1rem!important;" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

    <script src="custom/js/billpayment.js"></script>
    <script src="custom/js/common.js"></script>
</asp:Content>

