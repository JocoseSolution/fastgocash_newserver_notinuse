﻿using InstantPayServiceLib;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DMT_Manager_dmt_direct_fund_transfer : System.Web.UI.Page
{
    //private static string UserId { get; set; }
    public static string Mobile { get; set; }
    private static string RemitterId { get; set; }
    public static bool IsKycComplete = false;
    public static string KYCStatus = string.Empty;
    public static string IsKycNotCompleteStr { get; set; }
    public static bool ShowKYCForm = false;
    public string PayoutRemitterDetail { get; set; }
    public string PayoutRemitterBenDetail { get; set; }
    private static string RegId = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (!(Page.IsPostBack))
            {
                if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
                {
                    Session["Validate"] = Session["UID"].ToString();
                    Session["Mobile"] = Request.QueryString["mobile"] != null ? Request.QueryString["mobile"].ToString() : string.Empty;
                    Session["RemitterId"] = Request.QueryString["sender"] != null ? Request.QueryString["sender"].ToString() : string.Empty;

                    CheckDMTPayoutActiveOrNot(Session["UID"].ToString());
                }
                else
                {
                    Response.Redirect("/");
                }
            }
            if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
            {
                string UserId = Session["UID"].ToString();

                CheckDMTPayoutActiveOrNot(UserId);

                Mobile = Request.QueryString["mobile"] != null ? Request.QueryString["mobile"].ToString() : string.Empty;
                RemitterId = Request.QueryString["sender"] != null ? Request.QueryString["sender"].ToString() : string.Empty;

                if (string.IsNullOrEmpty(UserId) && string.IsNullOrEmpty(Mobile) && string.IsNullOrEmpty(RemitterId))
                {
                    Response.Redirect("/dmt-manager/dmt-direct.aspx");
                }
                else if (string.IsNullOrEmpty(Mobile) && string.IsNullOrEmpty(RemitterId))
                {
                    Response.Redirect("/dmt-manager/dmt-direct.aspx");
                }
                else
                {
                    IsKycComplete = false;
                    ShowKYCForm = false;

                    CheckPayoutKycComplete(UserId, Mobile, RemitterId);
                    BindPayOutRemitterWithBenDetail(UserId, Mobile, RemitterId);
                }
            }
            else
            {
                Response.Redirect("/");
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    private void CheckDMTPayoutActiveOrNot(string agentId)
    {
        try
        {
            Details det = new Details();
            DataSet ds = det.AgencyInfo(agentId);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string Agent_Payout_Status = ds.Tables[0].Rows[0]["Agent_Payout_Status"].ToString();
                if (Agent_Payout_Status.Trim() == "NOT ACTIVE")
                {
                    Session["Validate"] = null;
                    Session["Mobile"] = null;
                    Session["RemitterId"] = null;
                    Response.Redirect("/DMT-Manager/DMT-Home.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    private void CheckPayoutKycComplete(string agentid, string mobile, string remitterid)
    {
        try
        {
            //bool iskyc = InstantPay_PayoutService.CheckPayoutKycComplete(agentid, mobile, remitterid, ref RegId, ref ShowKYCForm, ref KYCStatus);
            DataTable dtRemitter = InstantPay_PayoutService.GetDMTDirectRemitter(mobile, agentid);
            if (dtRemitter != null && dtRemitter.Rows.Count > 0)
            {
                bool iskyc = dtRemitter.Rows[0]["IsKYC"].ToString().ToLower() == "false" ? false : true;
                KYCStatus = dtRemitter.Rows[0]["KYCStatus"].ToString();
                string isShowkyc = dtRemitter.Rows[0]["ShowKYCForm"].ToString();
                ShowKYCForm = isShowkyc.ToLower() == "true" ? true : false;
                RegId = dtRemitter.Rows[0]["Id"].ToString().ToLower();
                Session["RegId"] = RegId;

                if (iskyc)
                {
                    IsKycComplete = true;
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    private void BindPayOutRemitterWithBenDetail(string agentid, string mobile, string remitterid)
    {
        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                if (!string.IsNullOrEmpty(agentid))
                {
                    if (!string.IsNullOrEmpty(mobile) && !string.IsNullOrEmpty(remitterid))
                    {
                        RegId = HttpContext.Current.Session["RegId"] != null ? HttpContext.Current.Session["RegId"].ToString() : string.Empty;

                        DataTable BenDetail = new DataTable();
                        DataTable dtPayoutRemitter = InstantPay_PayoutService.GetPayoutRemitterDetails(RegId, agentid, mobile, remitterid, ref BenDetail);

                        if (dtPayoutRemitter != null && dtPayoutRemitter.Rows.Count > 0)
                        {
                            //Session["RegId"] = RegId;
                            Session["PayRemitterId"] = RemitterId;
                            Session["PayMobile"] = Mobile;

                            txtPayoutFirstName.Text = dtPayoutRemitter.Rows[0]["FirstName"].ToString();
                            txtPayoutLastName.Text = dtPayoutRemitter.Rows[0]["LastName"].ToString();
                            string sendername = txtPayoutFirstName.Text + " " + txtPayoutLastName.Text;
                            string address = dtPayoutRemitter.Rows[0]["Address"].ToString();
                            txtPayoutAddress.Text = address;
                            string iskyc = dtPayoutRemitter.Rows[0]["IsKYC"].ToString();
                            bool booliskyc = iskyc.ToLower() == "true" ? true : false;
                            string kycStatus = dtPayoutRemitter.Rows[0]["KYCStatus"].ToString();

                            PayoutRemitterDetail = SenderHTMLDetails(sendername, mobile, address, iskyc, kycStatus, dtPayoutRemitter.Rows[0]["Remark"].ToString());
                            if (booliskyc)
                            {
                                PayoutRemitterBenDetail = SenderBenHTMLDetails(BenDetail);
                            }
                        }
                    }
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    private string SenderHTMLDetails(string sendername, string mobile, string address, string iskyc, string kycStatus, string Remark)
    {
        StringBuilder sbSender = new StringBuilder();

        sbSender.Append("<div class='row boxSender'>");
        sbSender.Append("<div class='col-sm-6'><p class='p_botm'>Sender Details</p></div>");
        sbSender.Append("<div class='col-sm-6' style='padding: 2px;'><div class='row'><div class='col-sm-6'><p class='p_botm'>" + sendername.ToUpper() + " (" + mobile + ")</p></div><div class='col-sm-6 text-right'><p class='p_botm btn btn-sm' style='border: 1px solid #fff;color: #fff;cursor: pointer;padding: 6px;' id='UpdateLocalAddress'><i class='fa fa-edit'></i> Edit local Address</p></div></div></div>");
        sbSender.Append("</div>");

        sbSender.Append("<div class='row boxSenderdetail'>");
        sbSender.Append("<div class='col-sm-12'><div class='row'><div class='col-sm-5'><p class='pmargin'>Name</p></div><div class='col-sm-2'>:</div><div class='col-sm-5'><p class='pmargin'>" + sendername.ToUpper() + " (" + mobile + ")</p></div></div></div>");
        sbSender.Append("<div class='col-sm-12'><div class='row'><div class='col-sm-5'><p class='pmargin'>Current / Local Address</p></div><div class='col-sm-2'>:</div><div class='col-sm-5'><p class='pmargin'>" + address + "</p></div></div></div>");
        if (kycStatus.ToLower() == "completed")
        {
            if (iskyc.ToLower() == "false")
            {
                if (ShowKYCForm)
                {
                    sbSender.Append("<div class='col-sm-12'><div class='row'><div class='col-sm-5'><p class='pmargin'>KYC Status</p></div><div class='col-sm-2'>:</div><div class='col-sm-5'><p class='pmargin' style='color:#ff414d;font-weight: bold;'>PENDING</p></div></div></div>");
                }
                else
                {
                    sbSender.Append("<div class='col-sm-12'><div class='row'><div class='col-sm-5'><p class='pmargin'>KYC Status</p></div><div class='col-sm-2'>:</div><div class='col-sm-5'><p class='pmargin' style='color:#ff414d;font-weight: bold;'>WAITING FOR APPROVAL</p></div></div></div>");
                }
            }
            else
            {
                sbSender.Append("<div class='col-sm-12'><div class='row'><div class='col-sm-5'><p class='pmargin'>KYC Status</p></div><div class='col-sm-2'>:</div><div class='col-sm-5'><p class='pmargin text-success' style='font-weight: bold;'>" + kycStatus + "</p></div></div></div>");
            }
        }
        else
        {
            sbSender.Append("<div class='col-sm-12'><div class='row'><div class='col-sm-5'><p class='pmargin'>KYC Status</p></div><div class='col-sm-2'>:</div><div class='col-sm-5'><p class='pmargin' style='color:#ff414d;font-weight: bold;'>" + kycStatus + "</p></div></div></div>");
        }

        sbSender.Append("<div class='col-sm-12'><div class='row'><div class='col-sm-5'><p class='pmargin'>Remark</p></div><div class='col-sm-2'>:</div><div class='col-sm-5'><p class='pmargin'>" + (!string.IsNullOrEmpty(Remark) ? Remark : "- - -") + "</p></div></div></div>");

        sbSender.Append("</div>");

        return sbSender.ToString();
    }

    private string SenderBenHTMLDetails(DataTable dtBenDetail)
    {
        StringBuilder sbSender = new StringBuilder();

        if (dtBenDetail != null && dtBenDetail.Rows.Count > 0)
        {
            for (int i = 0; i < dtBenDetail.Rows.Count; i++)
            {
                string benid = dtBenDetail.Rows[i]["BenId"].ToString();

                sbSender.Append("<tr>");
                sbSender.Append("<td>" + dtBenDetail.Rows[i]["BenName"].ToString() + "</td>");
                //sbSender.Append("<td>Other</td>");
                sbSender.Append("<td>" + dtBenDetail.Rows[i]["BankName"].ToString() + "</td>");
                sbSender.Append("<td><span class='fa fa-check-circle accountvalid' data-toogle='tooltip' title='Account number is valid'></span></td>");
                //if (dtBenDetail.Rows[i]["Status"].ToString() == "1")
                //{
                //    sbSender.Append("<td><span class='fa fa-check-circle accountvalid' data-toogle='tooltip' title='Account number is valid'></span></td>");
                //}
                //else
                //{
                //    sbSender.Append("<td><span class='fa fa-question-circle text-warning accountvalid' data-toogle='tooltip' title='A/C validation is pending'></span></td>");
                //}
                sbSender.Append("<td>" + dtBenDetail.Rows[i]["AccountNo"].ToString() + "</td>");
                sbSender.Append("<td>" + dtBenDetail.Rows[i]["IFSCCode"].ToString() + "</td>");
                sbSender.Append("<td><select id='ddlTransferMode_" + benid + "' class='krishna'><option value='DPN'>IMPS</option><option value='BPN'>NEFT</option><option value='CPN'>RTGS</option></select></td>");
                sbSender.Append("<td><div class='form-validation'> <input type='text' class='form-control textboxamount krishna' style='width: 150px;' id='txtTranfAmount_" + benid + "' name='txtTranfAmount_" + benid + "' /></div></td>");
                sbSender.Append("<td><span class='btn btn-success btn-sm bentransfermoney' style='padding: 5px; font-size: 12px; font-weight: 500; color: #fff!important' id='btnMoneyTransfer_" + benid + "' data-benid='" + benid + "'  onclick=\"DirectTransferMoney('" + benid + "')\">Transfer</span></td>");
                sbSender.Append("<td><span class='btn btn-danger btn-sm bendeleterecord' style='padding: 5px; font-size: 12px; font-weight: 500; color: #fff!important' id='btnDeleteBeneficiary_" + benid + "' data-benid='" + benid + "' onclick=\"DeleteBeneficialRecord('" + benid + "')\">Delete</span></td>");
                sbSender.Append("</tr>");
            }
        }
        else
        {
            sbSender.Append("<tr>");
            sbSender.Append("<td colspan='9' class='text-center text-danger'>Record not found !</td>"); ;
            sbSender.Append("</tr>");
        }

        return sbSender.ToString();
    }

    protected void PayoutSubmitKYC_Click(object sender, EventArgs e)
    {
        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                    {
                        string fn = txtPayoutFirstName.Text;
                        string ln = txtPayoutLastName.Text;
                        string add = txtPayoutAddress.Text;
                        string gen = ddlPayoutGender.SelectedValue;
                        string dob = txtPayoutDOB.Text;
                        string proof = ddlPayoutProof.SelectedValue;
                        string pno = txtProofNumber.Text;

                        string folderPath = Server.MapPath("PayoutKYCImage/" + RemitterId + "/");
                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath);
                        }

                        fluPayoutFrontImg.SaveAs(folderPath + "frontimg.jpg");
                        fluPayoutBackImg.SaveAs(folderPath + "backimg.jpg");

                        string frontimg = "/dmt-manager/PayoutKYCImage/" + RemitterId + "/frontimg.jpg";
                        string backimg = "/dmt-manager/PayoutKYCImage/" + RemitterId + "/backimg.jpg";

                        if (InstantPay_PayoutService.UpdateRemitterKYCDetails(RegId, UserId, Mobile, RemitterId, fn, ln, add, gen, dob, proof, pno, frontimg, backimg))
                        {
                            Page_Load(sender, e);
                            ScriptManager.RegisterStartupScript(this, typeof(Page), "alert", "alert('KYC details has been submitted successfully, Please wait for approval.');", true);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    public static string GetLocalIPAddress()
    {
        string ipAddress = string.Empty;

        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                ipAddress = ip.ToString();
            }
        }

        return ipAddress;
    }

    [WebMethod]
    public static List<string> UpdateLocalAddress(string updatedaddress)
    {
        List<string> result = new List<string>();

        if (!string.IsNullOrEmpty(updatedaddress.Trim()))
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    if (InstantPay_PayoutService.PayoutUpdateLocalAddress(RegId, RemitterId, UserId, Mobile, updatedaddress))
                    {
                        result.Add("success");
                        result.Add("Address has been updated successfully.");
                    }
                }
            }
        }
        else
        {
            result.Add("empty");
            result.Add("Please enter current / local address !");
        }

        return result;
    }

    [WebMethod]
    public static List<string> PayoutBeneficiaryRegistration(string accountno, string bankname, string ifsccode, string name)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["Validate"].ToString();
                Mobile = HttpContext.Current.Session["Mobile"].ToString();
                RemitterId = HttpContext.Current.Session["RemitterId"].ToString();
                RegId = HttpContext.Current.Session["RegId"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                    {
                        bool isAdded = InstantPay_PayoutService.PayoutBeneficiaryRegistration(RegId, RemitterId, name, Mobile, accountno, bankname, ifsccode, UserId);

                        if (isAdded)
                        {
                            DataTable dtBenDetail = InstantPay_PayoutService.GetPayoutRemitterBenDetails(RegId, UserId, Mobile, RemitterId);
                            result.Add("success");

                            DMT_Manager_dmt_direct_fund_transfer bendetail = new DMT_Manager_dmt_direct_fund_transfer();
                            result.Add(bendetail.SenderBenHTMLDetails(dtBenDetail));
                        }
                        else
                        {
                            result.Add("failed");
                            result.Add("Error : Failed during beneficiary registration!");
                        }
                    }
                }
                else
                {
                    result.Add("reload");
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> GetPayoutFundTransferVeryficationDetail(string benid, string amount, string mode, string modetype)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                    {
                        StringBuilder sbTrans = new StringBuilder();

                        DataTable BenDetail = new DataTable();
                        DataTable dtPayoutRemitter = InstantPay_PayoutService.GetPayoutRemitterDetails(RegId, UserId, Mobile, RemitterId, ref BenDetail, benid);

                        if (dtPayoutRemitter != null && dtPayoutRemitter.Rows.Count > 0)
                        {
                            result.Add("success");

                            sbTrans.Append("<div class='row' style='border-bottom: 1px solid #dee2e6; padding: 0.7rem;'>");
                            sbTrans.Append("<div class='col-sm-4'><p>From</p><span>" + dtPayoutRemitter.Rows[0]["FirstName"].ToString() + " " + dtPayoutRemitter.Rows[0]["LastName"].ToString() + " (" + dtPayoutRemitter.Rows[0]["Mobile"].ToString() + ")</span><br/><span>" + dtPayoutRemitter.Rows[0]["Address"].ToString() + ", " + dtPayoutRemitter.Rows[0]["Pincode"].ToString() + "</span></div>");
                            sbTrans.Append("<div class='col-sm-4' style='text-align: center;'><img src='./images/arrowright.png' style='width: 120px;' /></div>");
                            sbTrans.Append("<div class='col-sm-4'><p>To</p><span>" + BenDetail.Rows[0]["BenName"].ToString() + "</span><br/><span>" + BenDetail.Rows[0]["BankName"].ToString() + " - (" + BenDetail.Rows[0]["IFSCCode"].ToString() + ")</span><br/><span>A/C - " + BenDetail.Rows[0]["AccountNo"].ToString() + "</span></div>");
                            sbTrans.Append("</div>");

                            sbTrans.Append("<div class='row' style='border-bottom: 1px solid #dee2e6; padding: 0.7rem;'>");

                            sbTrans.Append("<div class='col-sm-12'><div class='row'>");
                            sbTrans.Append("<div class='col-sm-4'><b>Amount to be Remitted</b></div>");
                            sbTrans.Append("<div class='col-sm-4'><b>Transfer Mode</b></div>");
                            sbTrans.Append("<div class='col-sm-4'><b>Amount to be Collected</b></div>");
                            sbTrans.Append("</div></div>");

                            sbTrans.Append("<div class='col-sm-12' style='padding: 15px;'><div class='row'>");
                            sbTrans.Append("<div class='col-sm-4'>₹ " + amount + "</div><div class='col-sm-4'>" + mode + "</div><div class='col-sm-4'>₹ " + amount + "</div>");
                            sbTrans.Append("</div></div>");

                            sbTrans.Append("</div>");

                            sbTrans.Append("<div class='row' style='padding: 1rem;'>");
                            sbTrans.Append("<div class='col-sm-1'><input type='checkbox' class='form-control' id='ChkFundTransCheckboxCheck' checked='checked' value='' style='height: 20px;' /></div>");
                            sbTrans.Append("<div class='col-sm-11'>By clicking the checkbox I accept the above declaraction.</div>");
                            sbTrans.Append("</div>");

                            result.Add(sbTrans.ToString());

                            result.Add("<div class='col-sm-12'><button type='button' class='btn btn-primary btn-sm' id='btnIMPSTrans' style='width: 100%;' data-benid='" + benid + "' data-transamount='" + amount + "' data-modetype='" + modetype + "' onclick='PayoutFundTransfer();'>DIRECT FUND TRANSFER</button></div>");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    private static bool BindOTPPrint(string agentid, string otp)
    {
        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["Validate"].ToString();

                if (!string.IsNullOrEmpty(UserId))
                {
                    StringBuilder sbResult = new StringBuilder();

                    if (!string.IsNullOrEmpty(UserId))
                    {
                        sbResult.Append("<div style='border: 1px solid #ff414d; width: 800px; border-collapse: initial;'>");
                        sbResult.Append("<div class='col-sm-12'>");
                        sbResult.Append("<a href='#'><img src='http://fastgocash.com/Images/gallery/logo(ft).png' style='max-width: 200px;' /></a>");
                        sbResult.Append("<hr style='text-align: center; border: 1px solid #ff414d; background-color: #ff414d; margin-top: 0px; margin-bottom: 0;' />");
                        sbResult.Append("</div>");
                        sbResult.Append("<div class='col-sm-12' style='padding:10px;'>");
                        sbResult.Append("<h4>Dear Sir/Madam,</h4><br />");
                        sbResult.Append("<b>Please use following OTP <span style='border: 1px solid #ccc;padding: 15px;'>" + otp + "</span> to complete your DTM DIRECT money transfer.</b>");
                        sbResult.Append("<br /><br />");
                        sbResult.Append("<p>This is a automatically genrated by the FastGoCash. Please don nor reply to this email.</p>");
                        sbResult.Append("</div>");
                        sbResult.Append("</div>");

                        DataTable dtAgency = InstantPay_ApiService.GetAgencyDetailById(UserId);
                        if (dtAgency != null && dtAgency.Rows.Count > 0)
                        {
                            string toEmail = dtAgency.Rows[0]["Email"].ToString();

                            SqlTransactionDom STDOM = new SqlTransactionDom();
                            DataTable MailDt = new DataTable();
                            MailDt = STDOM.GetMailingDetails("MONEY_TRANSFER", UserId).Tables[0];
                            if (MailDt != null && MailDt.Rows.Count > 0)
                            {
                                bool Status = Convert.ToBoolean(MailDt.Rows[0]["Status"].ToString());
                                string subject = "One Time Password (OTP) Confirmation For Transfer Money - Direct DMT";
                                if (Status)
                                {
                                    int isSuccess = STDOM.SendMail(toEmail, MailDt.Rows[0]["MAILFROM"].ToString(), MailDt.Rows[0]["BCC"].ToString(), MailDt.Rows[0]["CC"].ToString(), MailDt.Rows[0]["SMTPCLIENT"].ToString(), MailDt.Rows[0]["UserId"].ToString(), MailDt.Rows[0]["Pass"].ToString(), sbResult.ToString(), subject, "");
                                    if (isSuccess > 0)
                                    {
                                        return true;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return false;
    }

    [WebMethod]
    public static List<string> GenrateOTPForTransfer(string benid, string amount, string transmode)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                    {
                        string otp = string.Empty;
                        if (InstantPay_PayoutService.GenrateOTPForTransfer(RegId, UserId, Mobile, RemitterId, ref otp))
                        {
                            if (BindOTPPrint(UserId, otp))
                            {
                                result.Add("success");
                                string rutStr = "<div class='col-sm-12'><div class='row form-group form-validation'><textarea id='txtRemtRemark' class='form-control' placeholder='Enter Remark'></textarea></div><div class='row form-group'>"
                                    + "<div class='col-sm-4 '><input id='txtPayoutOTPInput' class='form-control row' placeholder='Enter OTP'></div>"
                                    + "<div class='col-sm-4 form-validation'><button type='button' class='btn btn-primary btn-md' id='btnPayoutOTPTrans' style='width: 100%;' data-benid='" + benid + "' data-transamount='" + amount + "' data-modetype='" + transmode + "'>TRANSFER</button></div>"
                                    + "<div class='col-sm-4' style='top: -25px;'><span id='btnPayoutResendOTP' style='cursor: pointer; font-size: 15px;' class='btn btn-primary btn-block my-4 hidden' data-benid='" + benid + "' data-transamount='" + amount + "' data-modetype='" + transmode + "' onclick='PayoutResendOTP();'>Resend OTP</span>"
                                    + "<span id='payouttimersection' class='btn btn-primary btn-block my-4 hidden'><span id='payminRemaing'></span>:<span id='paysecRemaing'></span></span>"
                                    + "</div></div></div>";
                                result.Add(rutStr);
                            }
                            else
                            {
                                result.Add("error");
                                result.Add("Try again !");
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> GenrateDeleteBeneficiaryOTP(string benid)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                    {
                        string otp = string.Empty;
                        if (InstantPay_PayoutService.GenrateOTPForBenDelete(UserId, Mobile, RemitterId, benid, ref otp))
                        {
                            result.Add("success");
                            result.Add(otp);
                        }
                        else
                        {
                            result.Add("failed");
                            result.Add("Error occured!");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> ProcessToPayoutFundTransfer(string benid, string amount, string transmode, string otp, string remark)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["Validate"].ToString();
                Mobile = HttpContext.Current.Session["Mobile"].ToString();
                RemitterId = HttpContext.Current.Session["RemitterId"].ToString();
                if (!string.IsNullOrEmpty(HttpContext.Current.Session["Validate"].ToString()))
                {
                    if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                    {
                        //DataTable BenDetail = new DataTable();
                        //DataTable dtPayoutRemitter = InstantPay_PayoutService.GetPayoutRemitterDetails(RegId, UserId, Mobile, RemitterId, ref BenDetail);

                        DataTable dtAgencyRemtBen = LedgerService.GetRemBenDetailByAgencyId(HttpContext.Current.Session["Validate"].ToString(), Mobile, RemitterId, benid);

                        if (dtAgencyRemtBen != null && dtAgencyRemtBen.Rows.Count > 0)
                        {
                            string trnasotp = !string.IsNullOrEmpty(dtAgencyRemtBen.Rows[0]["TransferOTP"].ToString()) ? dtAgencyRemtBen.Rows[0]["TransferOTP"].ToString() : string.Empty;
                            string trnasotptime = !string.IsNullOrEmpty(dtAgencyRemtBen.Rows[0]["TransferOTPExpDate"].ToString()) ? dtAgencyRemtBen.Rows[0]["TransferOTPExpDate"].ToString() : string.Empty;

                            if (!string.IsNullOrEmpty(trnasotp))
                            {
                                DateTime dtExpOtp = new DateTime();
                                dtExpOtp = Convert.ToDateTime(trnasotptime);

                                TimeSpan ts = DateTime.Now - dtExpOtp;

                                if (ts.Minutes < 3)
                                {
                                    if (trnasotp == otp)
                                    {
                                        if (InstantPay_PayoutService.RemoveOTPForTransfer(RegId, HttpContext.Current.Session["Validate"].ToString(), Mobile, RemitterId))
                                        {
                                            trnasotp = string.Empty;
                                            otp = string.Empty;

                                            string agencyName = string.Empty; string agencyCreditLimit = string.Empty;

                                            //DataTable dtAgency = InstantPay_ApiService.GetAgencyDetailById(UserId);

                                            //if (dtAgency != null && dtAgency.Rows.Count > 0)
                                            //{
                                            //string alert_email = !string.IsNullOrEmpty(dtAgency.Rows[0]["Email"].ToString()) ? dtAgency.Rows[0]["Email"].ToString() : "";
                                            //string alert_mobile = !string.IsNullOrEmpty(dtAgency.Rows[0]["Mobile"].ToString()) ? dtAgency.Rows[0]["Mobile"].ToString() : "";

                                            agencyName = dtAgencyRemtBen.Rows[0]["Agency_Name"].ToString();
                                            agencyCreditLimit = dtAgencyRemtBen.Rows[0]["Crd_Limit"].ToString();

                                            //if (Convert.ToDouble(agencyCreditLimit) > 0)
                                            if (Convert.ToDouble(agencyCreditLimit) >= Convert.ToDouble(amount))
                                            {
                                                //debit           
                                                string narration = amount + "_Payout_Direct_Money_Transfer_To_BeneficiaryId_" + benid;
                                                List<string> isLedger = LedgerDebitCreditSection(amount, HttpContext.Current.Session["Validate"].ToString(), agencyName, "Debit", "Payout Direct Money Transfer", "Payout_Direct_Money_Transfer", "DR", narration, "DEBIT NOTE", HttpContext.Current.Session["Validate"].ToString(), Mobile, RemitterId, benid);

                                                StringBuilder sbResult = new StringBuilder();

                                                if (isLedger.Count > 0)
                                                {
                                                    if (!string.IsNullOrEmpty(isLedger[0]))
                                                    {
                                                        string alert_email = "fastgocash@gmail.com";
                                                        string alert_mobile = "9760443317";

                                                        string avlBal = isLedger[1];
                                                        string trackid = isLedger[0];

                                                        if (!string.IsNullOrEmpty(trackid))
                                                        {
                                                            DataTable dtAgencyRemtBenCheck = LedgerService.GetRemBenDetailByAgencyId(HttpContext.Current.Session["Validate"].ToString(), Mobile, RemitterId, benid);
                                                            if (dtAgencyRemtBenCheck != null && dtAgencyRemtBenCheck.Rows.Count > 0)
                                                            {
                                                                sbResult.Append(BindUserSendFundTrasDetail(benid, trackid));

                                                                string response = InstantPay_PayoutService.PaayoutDirectMoneyTransfer(RegId, HttpContext.Current.Session["Validate"].ToString(), Mobile, RemitterId, benid, transmode, trackid, amount, GetLocalIPAddress(), remark, alert_email, alert_mobile);

                                                                if (!string.IsNullOrEmpty(response))
                                                                {
                                                                    dynamic dyResult = JObject.Parse(response);
                                                                    string statusCode = dyResult.statuscode;
                                                                    string statusMessage = dyResult.status;

                                                                    //if (statusCode.ToLower() != "txn" && statusMessage.ToLower() != "transaction successful")
                                                                    if ((statusCode.ToLower() != "txn" && statusMessage.ToLower() != "transaction successful") && (statusCode.ToLower() != "tup" && statusMessage.ToLower() != "transaction under process"))
                                                                    {
                                                                        string rufnarration = amount + "_Payout_Direct_Money_Transfer_To_BeneficiaryId_" + benid;
                                                                        List<string> isrufLedger = LedgerDebitCreditSection(amount, HttpContext.Current.Session["Validate"].ToString(), agencyName, "Credit", "Refund", "Payout Direct Money Transfer", "CR", rufnarration, "CREDIT NOTE", HttpContext.Current.Session["Validate"].ToString(), Mobile, RemitterId, benid);

                                                                        if (isLedger.Count > 0)
                                                                        {
                                                                            string rufavlBal = isrufLedger[1];
                                                                            string ruftrackid = isrufLedger[0];
                                                                            bool isRefund = InstantPay_ApiService.UpdateRefundStatusInstantPayInitiatePayout(trackid);
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    string rufnarration = amount + "_Payout_Direct_Money_Transfer_To_BeneficiaryId_" + benid;
                                                                    List<string> isrufLedger = LedgerDebitCreditSection(amount, HttpContext.Current.Session["Validate"].ToString(), agencyName, "Credit", "Refund", "Payout Direct Money Refund Transfer", "CR", rufnarration, "CREDIT NOTE", HttpContext.Current.Session["Validate"].ToString(), Mobile, RemitterId, benid);

                                                                    if (isLedger.Count > 0)
                                                                    {
                                                                        string rufavlBal = isrufLedger[1];
                                                                        string ruftrackid = isrufLedger[0];
                                                                        bool isRefund = InstantPay_ApiService.UpdateRefundStatusInstantPayInitiatePayout(trackid);
                                                                    }
                                                                }

                                                                sbResult.Append(BindFundTransactionResponseDetail(response, amount));
                                                                sbResult.Append("</div>");
                                                                sbResult.Append("</div>");

                                                                result.Add("success");
                                                                result.Add(sbResult.ToString() + "<div class='col-sm-12 text-right'><a href='/dmt-manager/dmt-direct-print.aspx?regid=" + RegId + "&mobile=" + Mobile + "&amp;sender=" + RemitterId + "&amp;beneficaryid=" + benid + "&amp;trackid=" + trackid + "' target='_blank' style='cursor: pointer;color:#fff;' class='col-sm-4 btn btn-primary '><i class='fa fa-print'></i> Print Receipt</a></div>");

                                                                string mailBody = BindTransPrint(benid, trackid);
                                                                MailSend(mailBody, trackid);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            result.Add("failed");
                                                            result.Add("ERROR OCCURED ! DEDUCT MONEY FROM YOUR WALLET !");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        result.Add("failed");
                                                        result.Add("ERROR OCCURED ! DEDUCT MONEY FROM YOUR WALLET !");
                                                    }
                                                }
                                                else
                                                {
                                                    result.Add("failed");
                                                    result.Add("WE ARE UNABLE TO TRANSFER MONEY AT THE MOMENT. INSTEAD OF TRYING AGAIN, PLEASE CONTACT OUR CALL CENTRE TO AVOID ANY INCONVENIENCE.");
                                                }
                                            }
                                            else
                                            {
                                                result.Add("failed");
                                                result.Add("INSUFFICIENT CREDIT BALANCE IN YOUR WALLET !");
                                            }
                                            //}
                                        }
                                    }
                                    else
                                    {
                                        result.Add("failed");
                                        result.Add("YOU HAVE ENTERED WRONG OTP !");
                                    }
                                }
                                else
                                {
                                    result.Add("otpexp");
                                    result.Add("OTP EXPIRED, PLEASE RESEND OTP !");
                                }
                            }
                            else
                            {
                                result.Add("otpexp");
                                result.Add("OTP EXPIRED, PLEASE RESEND OTP !");
                            }
                        }
                    }
                }
                else
                {
                    result.Add("reload");
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    private static string BindUserSendFundTrasDetail(string benid, string trackid)
    {
        StringBuilder sbResult = new StringBuilder();

        if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
        {
            string UserId = HttpContext.Current.Session["Validate"].ToString();
            Mobile = HttpContext.Current.Session["Mobile"].ToString();
            RemitterId = HttpContext.Current.Session["RemitterId"].ToString();
            RegId = HttpContext.Current.Session["RegId"].ToString();
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    DataTable BenDetail = new DataTable();
                    DataTable dtPayoutRemitter = InstantPay_PayoutService.GetPayoutRemitterDetails(RegId, UserId, Mobile, RemitterId, ref BenDetail, benid);

                    if (dtPayoutRemitter != null && dtPayoutRemitter.Rows.Count > 0)
                    {
                        string currDatetime = DateTime.Now.ToString();

                        sbResult.Append(" <div class='row' style='border-bottom: 1px solid #dee2e6; padding: 0.7rem;'>");
                        sbResult.Append("<div class='col-sm-4'><span><b>From</b></span><br/><span>" + dtPayoutRemitter.Rows[0]["FirstName"].ToString() + " " + dtPayoutRemitter.Rows[0]["LastName"].ToString() + " (" + dtPayoutRemitter.Rows[0]["Mobile"].ToString() + ")</span><br/><span>" + dtPayoutRemitter.Rows[0]["Address"].ToString() + ", " + dtPayoutRemitter.Rows[0]["Pincode"].ToString() + "</span></div>");

                        sbResult.Append("<div class='col-sm-4' style='text-align: center;'>");
                        sbResult.Append("<p class='text-center'>");
                        sbResult.Append("<span class='fa fa-check-circle text-center text-success' style='font-size: 50px;'></span><br />");
                        sbResult.Append("<span class='text-center' style='font-size: 20px;'>Track ID</span><br />");
                        sbResult.Append("<span class='text-center' style='font-size: 15px;color:#ff414d;'>" + trackid + "</span>");
                        //sbResult.Append("<br /><span style='color:#b0b0b0;'>" + ConvertStringDateToStringDateFormate(currDatetime) + "</span>");
                        sbResult.Append("</p>");
                        sbResult.Append("</div>");
                        sbResult.Append("<div class='col-sm-4'><span><b>To</b></span><br/><span>" + BenDetail.Rows[0]["BenName"].ToString() + "</span><br/><span>" + BenDetail.Rows[0]["BankName"].ToString() + " - (" + BenDetail.Rows[0]["IFSCCode"].ToString() + ")</span><br/><span>A/C - " + BenDetail.Rows[0]["AccountNo"].ToString() + "</span></div>");
                        sbResult.Append("</div>");

                        sbResult.Append("<div class='row' style='padding: 1rem;'>");

                        sbResult.Append("<div class='col-sm-12'>");
                        sbResult.Append("<div class='row' style='border-bottom: 1px dotted #ccc;'>");
                        sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'><b>ORDER ID</b></div>");
                        sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'><b>TRANSACTION DATE</b></div>");
                        sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'><b>STATUS</b></div>");
                        sbResult.Append("</div>");
                        sbResult.Append("</div>");

                        sbResult.Append("<div class='col-sm-12' style='padding: 15px;'>");
                    }
                }
            }
        }
        else
        {
            HttpContext.Current.Response.Redirect("~/Login.aspx", false);
        }

        return sbResult.ToString();
    }

    private static string BindFundTransactionResponseDetail(string response, string amount)
    {
        StringBuilder sbResult = new StringBuilder();

        if (!string.IsNullOrEmpty(response))
        {
            dynamic dyResult = JObject.Parse(response);
            string statusCode = dyResult.statuscode;
            string statusMessage = dyResult.status;

            string currDatetime = dyResult.timestamp;

            string isdatathere = string.Empty;
            JToken root = JObject.Parse(response);
            JToken data = root["data"];
            if (data != null)
            {
                isdatathere = data.ToString();

                if (!string.IsNullOrEmpty(isdatathere))
                {
                    dynamic dyData = dyResult.data;

                    string orderid = dyData.ipay_id;
                    string reqAmount = dyData.amount;
                    string debitAmount = dyData.charged_amt;
                    string balance = dyData.opening_bal;

                    sbResult.Append("<div class='row' style='margin-bottom: 10px; border-bottom: 1px dotted #ccc;'>");
                    sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'>" + orderid + "</div>");
                    sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'><span style='color:#b0b0b0;'>" + ConvertStringDateToStringDateFormate(currDatetime) + "</span></div>");

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        sbResult.Append("<div class='col-sm-4 text-success text-center' style='margin-bottom: 10px;font-weight: bold;'>SUCCESS</div>");
                    }
                    else if (statusCode.ToLower() == "tup" && statusMessage.ToLower() == "transaction under process")
                    {
                        sbResult.Append("<div class='col-sm-4 text-warning text-center' style='margin-bottom: 10px;font-weight: bold;'>UNDER PROCESS</div>");
                    }
                    else if (statusCode.ToLower() == "err" && statusMessage.ToLower() == "transaction failed")
                    {
                        sbResult.Append("<div class='col-sm-4 text-danger text-center' style='margin-bottom: 10px;font-weight: bold;'>FAILED</div>");
                    }
                    else if (statusCode.ToLower() == "dtx" && statusMessage.ToLower() == "duplicate transaction")
                    {
                        sbResult.Append("<div class='col-sm-4 text-danger text-center' style='margin-bottom: 10px;font-weight: bold;'>FAILED</div>");
                    }
                    sbResult.Append("</div>");
                }
                else
                {
                    sbResult.Append("<div class='row' style='margin-bottom: 10px; border-bottom: 1px dotted #ccc;'>");
                    sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'>" + statusMessage + "</div>");
                    sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'><span style='color:#b0b0b0;'>" + ConvertStringDateToStringDateFormate(currDatetime) + "</span></div>");
                    sbResult.Append("<div class='col-sm-4 text-danger text-center' style='margin-bottom: 10px;'>FAILED</div>");
                    sbResult.Append("</div>");
                }
            }
        }

        return sbResult.ToString();
    }

    #region [Ledger Deduct and Refund Section]
    private static List<string> LedgerDebitCreditSection(string amount, string agentid, string agencyName, string actiontype, string Remark, string shortRemark, string Uploadtype, string narration, string ledtype, string user_id, string mobile, string remitterId, string benid)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["Validate"].ToString();

                double Debit = actiontype.ToLower() == "debit" ? Convert.ToDouble(amount) : 0;
                double Credit = actiontype.ToLower() == "credit" ? Convert.ToDouble(amount) : 0;

                result = LedgerService.PayoutLedgerDebitCredit(Convert.ToDouble(amount), UserId, agencyName, agentid, GetLocalIPAddress(), Debit, Credit, actiontype, Remark, Uploadtype, shortRemark, narration, ledtype, user_id, mobile, remitterId, benid);
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    private static List<string> LedgerDebitCreditForCheckBen_DetailOnline(string amount, string agentid, string agencyName, string actiontype, string Remark, string shortRemark, string Uploadtype, string narration, string ledtype, string mobile, string remitterId)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["Validate"].ToString();

                double Debit = actiontype.ToLower() == "debit" ? Convert.ToDouble(amount) : 0;
                double Credit = actiontype.ToLower() == "credit" ? Convert.ToDouble(amount) : 0;

                result = LedgerService.LedgerDebitCreditForCheckBen_DetailOnline(Convert.ToDouble(amount), UserId, agencyName, agentid, GetLocalIPAddress(), Debit, Credit, actiontype, Remark, Uploadtype, shortRemark, narration, ledtype, mobile, remitterId);
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }
    #endregion

    private static bool MailSend(string mailBody, string TrackId)
    {
        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                    {
                        DataTable dtAgency = InstantPay_ApiService.GetAgencyDetailById(UserId);

                        if (dtAgency != null && dtAgency.Rows.Count > 0)
                        {
                            string toEmail = dtAgency.Rows[0]["Email"].ToString();

                            SqlTransactionDom STDOM = new SqlTransactionDom();
                            DataTable MailDt = new DataTable();
                            MailDt = STDOM.GetMailingDetails("MONEY_TRANSFER", UserId).Tables[0];
                            if (MailDt != null && MailDt.Rows.Count > 0)
                            {
                                bool Status = Convert.ToBoolean(MailDt.Rows[0]["Status"].ToString());
                                string subject = "Money Transfer Receipt [Client Ref# " + TrackId + "]";
                                if (Status)
                                {
                                    int isSuccess = STDOM.SendMail(toEmail, MailDt.Rows[0]["MAILFROM"].ToString(), MailDt.Rows[0]["BCC"].ToString(), MailDt.Rows[0]["CC"].ToString(), MailDt.Rows[0]["SMTPCLIENT"].ToString(), MailDt.Rows[0]["UserId"].ToString(), MailDt.Rows[0]["Pass"].ToString(), mailBody, subject, "");
                                    if (isSuccess > 0)
                                    {
                                        return true;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return false;
    }

    private static string BindTransPrint(string beneficaryid, string trackid)
    {
        StringBuilder sbResult = new StringBuilder();

        if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
        {
            string UserId = HttpContext.Current.Session["Validate"].ToString();
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    DataTable BenDetail = new DataTable();
                    DataTable dtPayoutRemitter = InstantPay_PayoutService.GetPayoutRemitterDetails(RegId, UserId, Mobile, RemitterId, ref BenDetail, beneficaryid);

                    sbResult.Append("<div style='border: 1px solid #ff414d; width: 800px; border-collapse: initial;'>");
                    sbResult.Append("<div class='col-sm-12'>");
                    sbResult.Append("<a href='#'><img src='http://fastgocash.com/Images/gallery/logo(ft).png' style='max-width: 200px;' /></a>");
                    sbResult.Append("<span style='float: right!important; text-align: right; padding: 7px;'>");
                    sbResult.Append("<span>" + dtPayoutRemitter.Rows[0]["FirstName"].ToString() + " " + dtPayoutRemitter.Rows[0]["LastName"].ToString() + "</span><br />");
                    sbResult.Append("<span>" + dtPayoutRemitter.Rows[0]["Address"].ToString() + ", " + dtPayoutRemitter.Rows[0]["Pincode"].ToString() + "</span>");
                    sbResult.Append("</span>");
                    sbResult.Append("<hr style='text-align: center; border: 1px solid #ff414d; background-color: #ff414d; margin-top: 0px; margin-bottom: 0;' />");
                    sbResult.Append("</div>");
                    sbResult.Append("<div class='col-sm-12'>");
                    sbResult.Append("<h4 style='text-align: center;'>Customer Transaction Receipt</h4>");
                    sbResult.Append("<table data-toggle='table' style='width: 800px; border-collapse: initial; border: 1px solid #ff414d; font-family: Verdana,Geneva,sans-serif; font-size: 12px; border-spacing: 0px; padding: 0px;'>");

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Sender Name</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtPayoutRemitter.Rows[0]["FirstName"].ToString() + " " + dtPayoutRemitter.Rows[0]["LastName"].ToString() + "</td>");
                    sbResult.Append("</tr>");

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Sender Mobile Number</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtPayoutRemitter.Rows[0]["Mobile"].ToString() + "</td>");
                    sbResult.Append("</tr>");

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Name</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + BenDetail.Rows[0]["BenName"].ToString() + "</td>");
                    sbResult.Append("</tr>");

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Account Number</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + BenDetail.Rows[0]["AccountNo"].ToString() + "</td>");
                    sbResult.Append("</tr>");

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Bank's IFSC</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + BenDetail.Rows[0]["IFSCCode"].ToString() + "</td>");
                    sbResult.Append("</tr>");

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Bank</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + BenDetail.Rows[0]["BankName"].ToString() + "</td>");
                    sbResult.Append("</tr>");

                    DataTable dtTrans = InstantPay_ApiService.GetTransactionHistoryByTrackId(trackid);

                    int transCount = dtTrans.Rows.Count;

                    if (transCount > 1)
                    {
                        decimal totalamount = 0;
                        for (int t = 0; t < dtTrans.Rows.Count; t++)
                        {
                            totalamount = totalamount + Convert.ToDecimal(dtTrans.Rows[t]["Amount"].ToString());
                        }

                        sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                        sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Amount</th>");
                        sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;font-weight:bold;'>₹ " + totalamount + "</td>");
                        sbResult.Append("</tr>");

                        sbResult.Append("<tr><th colspan='2'>&nbsp;</th><td colspan='2'>&nbsp;</td></tr>");
                        sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                        sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Order ID</th>");
                        sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Bank Reference ID</th>");
                        sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Amount</th>");
                        sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Date &amp; Times</th>");
                        sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Status</th>");
                        sbResult.Append("</tr>");

                        for (int t = 0; t < dtTrans.Rows.Count; t++)
                        {
                            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                            sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[t]["orderid"].ToString() + "</td>");
                            sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[t]["ref_no"].ToString() + "</td>");
                            sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px;font-weight:bold;'>₹ " + dtTrans.Rows[t]["Amount"].ToString() + "</td>");
                            sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[t]["UpdatedDate"].ToString()) + "</td>");
                            if (!string.IsNullOrEmpty(dtTrans.Rows[t]["Status"].ToString()) && dtTrans.Rows[t]["Status"].ToString().ToLower() == "transaction successful")
                            {
                                sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px; color:#28a745; font-weight:bold;'>" + dtTrans.Rows[t]["Status"].ToString() + "</td>");
                            }
                            else if (!string.IsNullOrEmpty(dtTrans.Rows[t]["Status"].ToString()))
                            {
                                sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px; color:#ff414d; font-weight:bold;'>" + dtTrans.Rows[t]["Status"].ToString() + "</td>");
                            }
                            else
                            {
                                sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px; color:#ff414d; font-weight:bold;'>Transaction Failed</td>");
                            }
                            sbResult.Append("</tr>");
                        }
                    }
                    else
                    {
                        sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                        sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Order ID</th>");
                        sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["orderid"].ToString() + "</td>");
                        sbResult.Append("</tr>");

                        //sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                        //sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction ID</th>");
                        //sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["ipay_id"].ToString() + "</td>");
                        //sbResult.Append("</tr>");

                        sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                        sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Track ID</th>");
                        sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["TrackId"].ToString() + "</td>");
                        sbResult.Append("</tr>");

                        sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                        sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Amount</th>");
                        sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>₹ " + dtTrans.Rows[0]["Amount"].ToString() + "</td>");
                        sbResult.Append("</tr>");

                        //sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                        //sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Charged Amount</th>");
                        //sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["charged_amt"].ToString() + "</td>");
                        //sbResult.Append("</tr>");

                        sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                        sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Date & Times</th>");
                        sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[0]["UpdatedDate"].ToString()) + "</td>");
                        sbResult.Append("</tr>");

                        sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                        sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Status</th>");
                        sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["Status"].ToString() + "</td>");
                        sbResult.Append("</tr>");
                    }

                    sbResult.Append("</table>");
                    sbResult.Append("<h4 style='padding-left: 5px;'>Note :-</h4>");
                    sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>1. Customer transaction charge is minimum of Rs. 10/- and Maximum 1% of the transaction amount.</p><br />");
                    sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>2. In case of non-payment to the beneficiary, the customer will receive an SMS with an OTP that he/she needs to present at the agent location where the transaction was initiated.</p><br />");
                    sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>3. In case the Agent charges the Customer in excess of the fee/ charges as mentioned in the receipt, he/she should lodge complaint about the same with our Customer Care on Tel. No. 7409455555 or email us at fastgocash@gmail.com.</p><br />");
                    sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>4. The receipt is subject to terms and conditions, privacy policy and terms of use detailed in the website www.fastgocash.com and shall be binding on the Customer for each transaction.</p><br /><br />");
                    sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>5. This is a system generated receipt hence does not require any signature.</p><br /><br />");

                    sbResult.Append("</div>");
                    sbResult.Append("</div>");
                }
            }
        }

        return sbResult.ToString();
    }

    public static string ConvertStringDateToStringDateFormate(string date)
    {
        DateTime dtDate = new DateTime();

        if (!string.IsNullOrEmpty(date))
        {
            dtDate = DateTime.Parse(date);
            return dtDate.ToString("dd MMM yyyy hh:mm tt");
        }

        return string.Empty;
    }

    [WebMethod]
    public static List<string> GetTransactionHistory()
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                    {
                        string fromdate = string.Empty;
                        DateTime currDate = DateTime.Now;
                        fromdate = currDate.ToString("dd/MM/yyyy");

                        DataTable dtTrans = InstantPay_PayoutService.GetFilterTransactionHistory(RemitterId, fromdate, "", "", "");

                        result.Add("success");
                        result.Add(BindTransDetails(dtTrans, "today"));
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    private static string BindTransDetails(DataTable dtTrans, string datepos)
    {
        StringBuilder sbTrans = new StringBuilder();

        if (dtTrans != null && dtTrans.Rows.Count > 0)
        {
            for (int i = 0; i < dtTrans.Rows.Count; i++)
            {
                string refund_html = "- - -";
                string status = dtTrans.Rows[i]["Status"].ToString();

                bool isRefund = dtTrans.Rows[i]["Refund"].ToString().ToLower() == "true" ? true : false;

                if (status.ToLower().Trim() == "transaction under process")
                {
                    status = "<td class='text-warning'>UNDER PROCESS</td>";
                }
                else if (status.ToLower().Trim() == "transaction successful")
                {
                    status = "<td class='text-success'>SUCCESS</td>";
                }
                else if (status.ToLower().Trim() == "duplicate transaction")
                {
                    status = "<td class='text-danger'>DUPLICATE</td>";
                    if (isRefund)
                    {
                        refund_html = "<span style='color: blue;font-weight: bold;'>REVERSAL</span>";
                    }
                    else
                    {
                        refund_html = "<span class='btn btn-primary' data-reportid='" + dtTrans.Rows[i]["RefundId"].ToString() + "' data-amount='" + dtTrans.Rows[i]["Amount"].ToString() + "' data-transid='" + dtTrans.Rows[i]["FundTransId"].ToString() + "' data-trackid='" + dtTrans.Rows[i]["TrackId"].ToString() + "' id='btnRefund_" + dtTrans.Rows[i]["FundTransId"].ToString() + "' onclick='RefundFaildAmount(" + dtTrans.Rows[i]["FundTransId"].ToString() + ");' style='padding: 0rem 0.3rem;font-size: 12px;'>Refund</span>";
                    }
                }
                else if (status.ToLower().Trim() == "transaction failed")
                {
                    status = "<td class='text-danger'>FAILED</td>";
                    if (isRefund)
                    {
                        refund_html = "<span style='color: blue;font-weight: bold;'>REVERSAL</span>";
                    }
                    else
                    {
                        refund_html = "<span class='btn btn-primary' data-reportid='" + dtTrans.Rows[i]["RefundId"].ToString() + "' data-amount='" + dtTrans.Rows[i]["Amount"].ToString() + "' data-transid='" + dtTrans.Rows[i]["FundTransId"].ToString() + "' data-trackid='" + dtTrans.Rows[i]["TrackId"].ToString() + "' id='btnRefund_" + dtTrans.Rows[i]["FundTransId"].ToString() + "' onclick='RefundFaildAmount(" + dtTrans.Rows[i]["FundTransId"].ToString() + ");' style='padding: 0rem 0.3rem;font-size: 12px;'>Refund</span>";
                    }
                }
                else
                {
                    status = "<td class='text-danger'>FAILED</td>";
                    if (isRefund)
                    {
                        refund_html = "<span style='color: blue;font-weight: bold;'>REVERSAL</span>";
                    }
                    else
                    {
                        refund_html = "<span class='btn btn-primary' data-reportid='" + dtTrans.Rows[i]["RefundId"].ToString() + "'  data-amount='" + dtTrans.Rows[i]["Amount"].ToString() + "' data-transid='" + dtTrans.Rows[i]["FundTransId"].ToString() + "' data-trackid='" + dtTrans.Rows[i]["TrackId"].ToString() + "' id='btnRefund_" + dtTrans.Rows[i]["FundTransId"].ToString() + "' onclick='RefundFaildAmount(" + dtTrans.Rows[i]["FundTransId"].ToString() + ");' style='padding: 0rem 0.3rem;font-size: 12px;'>Refund</span>";
                    }
                }

                sbTrans.Append("<tr>");
                sbTrans.Append("<td>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[i]["UpdatedDate"].ToString()) + "</td>");
                sbTrans.Append("<td>" + (!string.IsNullOrEmpty(dtTrans.Rows[i]["ipay_id"].ToString()) ? dtTrans.Rows[i]["ipay_id"].ToString() : "- - -") + "</td>");
                //sbTrans.Append("<td>" + dtTrans.Rows[i]["orderid"].ToString() + "</td>");
                //sbTrans.Append("<td>" + dtTrans.Rows[i]["ref_no"].ToString() + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["TrackId"].ToString() + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["TxnMode"].ToString() + "</td>");
                sbTrans.Append("<td>" + (!string.IsNullOrEmpty(dtTrans.Rows[i]["Amount"].ToString()) ? dtTrans.Rows[i]["Amount"].ToString() : "- - -") + "</td>");
                //sbTrans.Append("<td>" + (!string.IsNullOrEmpty(dtTrans.Rows[i]["charged_amt"].ToString()) ? dtTrans.Rows[i]["charged_amt"].ToString() : "- - -") + "</td>");

                if (string.IsNullOrEmpty(dtTrans.Rows[i]["BenificieryId"].ToString()))
                {
                    sbTrans.Append("<td><span style='padding: 0.1rem 0.3rem;color: #fff!important;background: #0000ff94 !important;border: #0000ff94 !important;box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.15);border-radius: 4px;' title='View'>" + (!string.IsNullOrEmpty(dtTrans.Rows[i]["BenName"].ToString()) ? dtTrans.Rows[i]["BenName"].ToString() : "- - -") + "</span></td>");
                    //url = "/dmt-manager/dmt-direct-ben-print.aspx?transid=" + dtTrans.Rows[i]["FundTransId"].ToString() + "&regid=" + RegId + "&mobile=" + Mobile + "&sender=" + RemitterId + "&trackid=" + dtTrans.Rows[i]["TrackId"].ToString() + "";
                }
                else
                {
                    sbTrans.Append("<td><span class='text-primary' title='View' style='cursor: pointer;padding: 0.1rem 0.3rem;color: #fff!important;background: #ff434f !important;border: #ff434f !important;box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.15);border-radius: 4px;' onclick=\"ShowBenDetails('" + dtTrans.Rows[i]["BenificieryId"].ToString() + "')\">" + dtTrans.Rows[i]["BenName"].ToString() + "</span></td>");
                }
                string url = "/dmt-manager/dmt-direct-print.aspx?transid=" + dtTrans.Rows[i]["FundTransId"].ToString() + "&regid=" + RegId + "&mobile=" + Mobile + "&sender=" + RemitterId + "&beneficaryid=" + dtTrans.Rows[i]["BenificieryId"].ToString() + "&trackid=" + dtTrans.Rows[i]["TrackId"].ToString() + "";
                sbTrans.Append(status);
                sbTrans.Append("<td>" + refund_html + "</td>");


                sbTrans.Append("<td><a href='" + url + "' target='_blank' class='text-primary'>Print</a></td>");
                sbTrans.Append("</tr>");
            }
        }
        else
        {
            if (datepos == "today")
            {
                sbTrans.Append("<tr>");
                sbTrans.Append("<td colspan='12' class='text-danger text-center'>Today's record not found !</td>");
                sbTrans.Append("</tr>");
            }
            else
            {
                sbTrans.Append("<tr>");
                sbTrans.Append("<td colspan='12' class='text-danger text-center'>Record not found !</td>");
                sbTrans.Append("</tr>");
            }
        }

        return sbTrans.ToString();
    }

    [WebMethod]
    public static List<string> GetFilterTransactionHistory(string fromdate, string todate, string trackid, string filstatus)
    {
        List<string> result = new List<string>();

        if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
        {
            string UserId = HttpContext.Current.Session["Validate"].ToString();
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    DataTable dtTrans = InstantPay_PayoutService.GetFilterTransactionHistory(RemitterId, fromdate, todate, trackid, filstatus);

                    result.Add("success");
                    result.Add(BindTransDetails(dtTrans, "filter"));
                }
            }
        }

        return result;
    }

    [WebMethod]
    public static string GetBeneficiaryDetailById(string benid)
    {
        StringBuilder result = new StringBuilder();

        if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
        {
            string UserId = HttpContext.Current.Session["Validate"].ToString();
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    if (!string.IsNullOrEmpty(benid))
                    {
                        DataTable dtBenDetail = InstantPay_PayoutService.GetPayoutRemitterBenDetails(RegId, UserId, Mobile, RemitterId, benid);
                        if (dtBenDetail != null && dtBenDetail.Rows.Count > 0)
                        {
                            result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>Beneficary ID</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["BenId"].ToString() + "</div></div>");
                            result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>Name</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["BenName"].ToString() + "</div></div>");
                            result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>Account</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["AccountNo"].ToString() + "</div></div>");
                            result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>IFSC Code</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["IFSCCode"].ToString() + "</div></div>");
                            result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>Bank</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["BankName"].ToString() + "</div></div>");
                        }
                    }
                }
            }
        }

        return result.ToString();
    }

    [WebMethod]
    public static List<string> DeleteBeneficiaryDetailById(string benid, string otp)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                    {
                        if (!string.IsNullOrEmpty(benid))
                        {
                            DataTable benDetail = InstantPay_PayoutService.GetPayoutRemitterBenDetails(string.Empty, UserId, Mobile, RemitterId, benid);

                            if (benDetail != null && benDetail.Rows.Count > 0)
                            {
                                string trnasotp = benDetail.Rows[0]["OTP"].ToString();
                                string trnasotptime = benDetail.Rows[0]["EXPOTP"].ToString();

                                DateTime dtExpOtp = new DateTime();
                                dtExpOtp = Convert.ToDateTime(trnasotptime);

                                TimeSpan ts = DateTime.Now - dtExpOtp;

                                if (ts.Minutes < 3)
                                {
                                    if (trnasotp == otp)
                                    {
                                        if (InstantPay_PayoutService.DeleteBeneficiaryDetailById(benid))
                                        {
                                            result.Add("success");
                                            result.Add(OnlySenderBenHTMLDetails(InstantPay_PayoutService.GetPayoutRemitterBenDetails(RegId, UserId, Mobile, RemitterId)));
                                        }
                                    }
                                    else
                                    {
                                        result.Add("failed");
                                        result.Add("You Have Entered Wrong OTP !");
                                    }
                                }
                                else
                                {
                                    result.Add("otpexp");
                                    result.Add("OTP Expired, Please Resend OTP !");
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    private static string OnlySenderBenHTMLDetails(DataTable dtBenDetail)
    {
        StringBuilder sbSender = new StringBuilder();

        if (dtBenDetail != null && dtBenDetail.Rows.Count > 0)
        {
            for (int i = 0; i < dtBenDetail.Rows.Count; i++)
            {
                string benid = dtBenDetail.Rows[i]["BenId"].ToString();

                sbSender.Append("<tr>");
                sbSender.Append("<td>" + dtBenDetail.Rows[i]["BenName"].ToString() + "</td>");
                sbSender.Append("<td>" + dtBenDetail.Rows[i]["BankName"].ToString() + "</td>");
                sbSender.Append("<td><span class='fa fa-check-circle accountvalid' data-toogle='tooltip' title='Account number is valid'></span></td>");
                sbSender.Append("<td>" + dtBenDetail.Rows[i]["AccountNo"].ToString() + "</td>");
                sbSender.Append("<td>" + dtBenDetail.Rows[i]["IFSCCode"].ToString() + "</td>");
                sbSender.Append("<td><select id='ddlTransferMode_" + benid + "' class='krishna'><option value='DPN'>IMPS</option><option value='BPN'>NEFT</option><option value='CPN'>RTGS</option></select></td>");
                sbSender.Append("<td><div class='form-validation'> <input type='text' class='form-control textboxamount krishna' style='width: 150px;' id='txtTranfAmount_" + benid + "' name='txtTranfAmount_" + benid + "' /></div></td>");
                sbSender.Append("<td><span class='btn btn-success btn-sm bentransfermoney' style='padding: 5px; font-size: 12px; font-weight: 500; color: #fff!important' id='btnMoneyTransfer_" + benid + "' data-benid='" + benid + "'  onclick=\"DirectTransferMoney('" + benid + "')\">Transfer</span></td>");
                sbSender.Append("<td><span class='btn btn-danger btn-sm bendeleterecord' style='padding: 5px; font-size: 12px;cursor: pointer; font-weight: 500; color: #fff!important;' id='btnDeleteBeneficiary_" + benid + "' data-benid='" + benid + "' onclick=\"DeleteBeneficialRecord('" + benid + "')\">Delete</span></td>");
                sbSender.Append("</tr>");
            }
        }
        else
        {
            sbSender.Append("<tr>");
            sbSender.Append("<td colspan='9' class='text-center text-danger'>Record not found !</td>"); ;
            sbSender.Append("</tr>");
        }

        return sbSender.ToString();
    }

    [WebMethod]
    public static List<string> GetBeneficiaryNamePayout(string accountno, string transtype, string ifsccode, string bankname, string paytype)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["Validate"].ToString();
                Mobile = HttpContext.Current.Session["Mobile"].ToString();
                RemitterId = HttpContext.Current.Session["RemitterId"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                    {
                        //DataTable dtAgency = InstantPay_ApiService.GetAgencyDetailById(UserId);

                        DataTable dtAgencyRemtBen = LedgerService.GetRemBenDetailByAgencyId(UserId, Mobile, RemitterId, "");

                        string latitude = "28.690430";
                        string longitude = "77.101662";
                        string ipaddress = GetLocalIPAddress();

                        if (dtAgencyRemtBen != null && dtAgencyRemtBen.Rows.Count > 0)
                        {
                            //string alert_email = !string.IsNullOrEmpty(dtAgency.Rows[0]["Email"].ToString()) ? dtAgency.Rows[0]["Email"].ToString() : "";
                            //string alert_mobile = !string.IsNullOrEmpty(dtAgency.Rows[0]["Mobile"].ToString()) ? dtAgency.Rows[0]["Mobile"].ToString() : "";

                            string agencyMobile = dtAgencyRemtBen.Rows[0]["Mobile"].ToString();
                            string agencyName = dtAgencyRemtBen.Rows[0]["Agency_Name"].ToString();
                            string agencyCreditLimit = dtAgencyRemtBen.Rows[0]["Crd_Limit"].ToString();

                            if (Convert.ToDouble(agencyCreditLimit) >= 3)
                            {
                                string narration = "3_To_Check_Beneficiary_Name";
                                List<string> isLedger = LedgerDebitCreditForCheckBen_DetailOnline("3", UserId, agencyName, "Debit", "getname", "Beneficiary_Name", "DR", narration, "DEBIT NOTE", Mobile, RemitterId);

                                StringBuilder sbResult = new StringBuilder();

                                if (isLedger.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(isLedger[0]))
                                    {
                                        string alert_email = "fastgocash@gmail.com";
                                        string alert_mobile = "9760443317";

                                        string avlBal = isLedger[1];
                                        string trackid = isLedger[0];
                                        string response = InstantPay_ApiService.InitiatePayout(Mobile, trackid, accountno, transtype, ifsccode, latitude, longitude, ipaddress, alert_email, alert_mobile, UserId, RemitterId, bankname, paytype);

                                        if (!string.IsNullOrEmpty(response))
                                        {
                                            dynamic dyResult = JObject.Parse(response);
                                            string statusCode = dyResult.statuscode;
                                            string statusMessage = dyResult.status;

                                            if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                                            {
                                                dynamic dyData = dyResult.data;
                                                dynamic dypayout = dyData.payout;

                                                string benName = dypayout.name;
                                                result.Add("success");
                                                result.Add(benName);
                                            }
                                            else
                                            {
                                                string rufnarration = "3_To_Check_Beneficiary_Name_Refund";
                                                List<string> isrufLedger = LedgerDebitCreditForCheckBen_DetailOnline("3", UserId, agencyName, "Credit", "refund", "Beneficiary_Name", "CR", rufnarration, "CREDIT NOTE", Mobile, RemitterId);

                                                if (isLedger.Count > 0)
                                                {
                                                    string rufavlBal = isrufLedger[1];
                                                    string ruftrackid = isrufLedger[0];
                                                    bool isRefund = InstantPay_ApiService.UpdateRefundStatusInstantPayInitiatePayout(trackid);
                                                }

                                                result.Add("failed");
                                                result.Add(statusMessage);
                                            }
                                        }
                                        else
                                        {
                                            string rufnarration = "3_To_Check_Beneficiary_Name_Refund";
                                            List<string> isrufLedger = LedgerDebitCreditForCheckBen_DetailOnline("3", UserId, agencyName, "Credit", "refund", "Beneficiary_Name", "CR", rufnarration, "CREDIT NOTE", Mobile, RemitterId);

                                            if (isLedger.Count > 0)
                                            {
                                                string rufavlBal = isrufLedger[1];
                                                string ruftrackid = isrufLedger[0];
                                                bool isRefund = InstantPay_ApiService.UpdateRefundStatusInstantPayInitiatePayout(trackid);
                                            }

                                            result.Add("failed");
                                            result.Add("Error : API Error !");
                                        }
                                    }
                                    else
                                    {
                                        result.Add("failed");
                                        result.Add("ERROR OCCURED ! DEDUCT MONEY FROM YOUR WALLET !");
                                    }
                                }
                            }
                            else
                            {
                                result.Add("failed");
                                result.Add("Insufficient credit limit in your wallet !");
                            }
                        }
                    }
                }
                else
                {
                    result.Add("reload");
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.ToString());

        }

        return result;
    }
}