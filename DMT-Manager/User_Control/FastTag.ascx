﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FastTag.ascx.cs" Inherits="DMT_Manager_User_Control_FastTag" %>

<h5 style="color: #ff5858;">Pay Your Fast Tag Bill</h5>
<br />
<div class="row">    
    <div class="col-md-3" id="BindFastTagSection">
        <label class="lbl-hide"></label>
        <select id="ddlFastTag" class="custom-select"></select>
    </div> 
    <div class="col-md-2">
        <label class="lbl-hide"></label>
        <span class="btn btn-primary btn-block" id="btnFastTag" onclick="return FeatchFastTagBill();">Get Bill</span>
    </div>
</div>

<br />
<div class="row hidden" id="fasttagnotesmsg"></div>


<button type="button" class="btn btn-info btn-lg hidden fasttagmodelclickclass" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#FastTagModelSection"></button>
<div class="modal fade" id="FastTagModelSection" role="dialog"><div class="modal-dialog" style="margin: 7% auto!important;"><div class="modal-content" id="modelfasttagheadbody"></div></div></div>
