﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmiLoan.ascx.cs" Inherits="DMT_Manager_User_Control_EmiLoan" %>


<h5 style="color: #ff5858;">Pay Your Loan RePayment Bill</h5>
<br />
<div class="row">    
    <div class="col-md-3" id="BindEMITagSection">
        <label class="lbl-hide"></label>
        <select id="ddlEMI" class="custom-select"></select>
    </div> 
    <div class="col-md-2">
        <label class="lbl-hide"></label>
        <span class="btn btn-primary btn-block" id="btnEMI" onclick="return FeatchEMIBill();">Get Bill</span>
    </div>
</div>

<br />
<div class="row hidden" id="eminotesmsg"></div>

<button type="button" class="btn btn-info btn-lg hidden emimodelclickclass" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#EMIModelSection"></button>
<div class="modal fade" id="EMIModelSection" role="dialog"><div class="modal-dialog" style="margin: 7% auto!important;"><div class="modal-content" id="modelemiheadbody"></div></div></div>
